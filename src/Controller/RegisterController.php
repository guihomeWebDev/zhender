<?php

namespace App\Controller;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class RegisterController extends AbstractController
{
    /**
     * @Route("/inscription", name="register")
     */
    public function index(Request $request, EntityManagerInterface $em): Response
    {
        $user =new User();
        $form = $this->createForm(registerType::class, $user);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isvalid()){
            $user = $form->getData();
            $em->persist($user);
            $em->flush;
        }

        return $this->render('register/index.html.twig', [
            'form' => $form->createview,
        ]);
    }
}
